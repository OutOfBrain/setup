#!/usr/bin/env bash
./install-software.sh
mv ./.tmux.conf ~/.tmux.conf
(./setup-keys.sh)
echo "source setup/aliases.sh" >> ~/.bashrc
source ~/.bashrc
(./setup-mybuntu.sh)
(./setup-repos.sh)
