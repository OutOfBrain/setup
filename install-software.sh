#!/usr/bin/env bash
set +x

apt-get update
apt-get install -y \
git \
curl \
sudo \
nano \
nodejs \
build-essential \
git \
sysbench \
vim \
openjdk-8-jdk \
youtube-dl \
tmux \
php7.0 \
ant \
make \
ffmpeg \
dstat \
htop \
ruby-dev \
wget \
ncdu \
rsync \
silversearcher-ag \
maven \
pv \
gitstats \
ssh \
iputils-ping \
php-curl \
unrar \
uuid-runtime

apt-get purge docker lxc-docker docker-engine docker.io
apt-get install -y curl apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce
