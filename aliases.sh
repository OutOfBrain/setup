#!/usr/bin/env bash

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000000
HISTFILESIZE=2000000


# pushes the current directory globally
function changedirhook {
	pwd > /tmp/currentlocation
}

# navigates to same directory as the previous command - useful to get multiple terminals to the same location
# c - shortform of cd
function c {
	cd $(cat /tmp/currentlocation)
}

PROMPT_COMMAND=changedirhook


alias mybuntu='docker run --rm -it -v $PWD:/w --workdir /w mybuntu:xenial bash'
alias mcl='mvn clean install'
alias dff='df -H /'
alias gstat='git diff --stat'
alias ll='ls -alhF'
alias la='ls -A'
alias l='ls -CF'
alias tm='cd /var/www/tm/'
alias youtube-dl='youtube-dl --prefer-ffmpeg'
