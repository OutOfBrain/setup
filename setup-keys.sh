#!/usr/bin/env bash
cd
git clone https://gitlab.com/OutOfBrain/mykeys.git
(cd mykeys && git pull)
mkdir ~/.ssh
mv mykeys/authorized_keys ~/.ssh/authorized_keys
